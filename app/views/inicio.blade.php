<?php  
//NOTAS: Se va a leer el documento por completo y se va a hacer una busqueda
//en la base de datos si existe al menos un control con la ID del paciente
//obtenida al buscar en la DB por el rut puesto en el archivo. 

//si no existe el rut, el paciente no existe, se crea o se cancela?
//personalmente pienso que se debería omitir, dado que por defecto la
//hoja de excel tiene un montón de campos vacíos hacia abajo, todos
//los cuales suponen un rut que no existe, de todas formas se puede
//agregar una distinción clara entre los que simplemente tienen rut = void
//y los que tienen un rut que no se encuentra, por ahora se omiten.

//En cualquier caso, despues de obtener el ID del paciente se va a hacer
//otra busqueda en la DB para saber si existen controles hechos, de ya existir
//por lo menos un control, se van a sobreescribir TODOS los datos de los
//examenes con los que fueron leidos desde el archivo, de no encontrarse 
//ningun control, se van a crear todos los controles de todas formas, tengan
//datos o no.

//Consulta nutricionista fue concatenado con un ; y el taller nutricionista

//FALTA la columna de abandono y motivo y año

//validar rut, si no existe, se crea y se le asigna, correspondientemente

	if($documento['Pacientes'][0][1] === 'Paciente'
		and $documento['Pacientes'][0][10] === 'Control Ingreso'
		and $documento['Pacientes'][0][28] === 'Control 4 Mes'
		and $documento['Pacientes'][0][75] === 'Control 12 Mes'
		and $documento['Pacientes'][0][64] === 'Control 9 Mes'
		and $documento['Pacientes'][1][1] === 'Datos'
		and $documento['Pacientes'][1][46] === 'Sesiones'
		and $documento['Pacientes'][1][48] === 'Talleres'
		and $documento['Pacientes'][1][75] === 'Alta / Abandono'
		and $documento['Pacientes'][2][1] === 'Rut'
		and $documento['Pacientes'][2][4] === 'Apellido Materno'
		and $documento['Pacientes'][2][39] === ' Colesterol Total'
		and $documento['Pacientes'][2][43] === 'Glicemia en  Ayuna'
		and $documento['Pacientes'][2][62] === '  IMC'
		and $documento['Pacientes'][2][65] === 'Motivo '){
	    echo "Documento Valido<BR>";
		foreach ($documento['Pacientes'] as $line ) {
			if($line[1] != 'Paciente' 
				and $line[1] != 'Datos' 
				and $line[1] != 'Rut'){
				$paciente = Paciente::Rut($line[1])->get()->first();
				if($paciente === NULL){echo "Paciente de rut ".$line[1]." no encontrado <BR>";}
				else{ 
					$parametros = ["pacientes_id" => $paciente->id,
					"tipo_control" => 'ingreso'];
					$Planilla = Control::firstOrNew($parametros);
					$Planilla->pacientes_id = $paciente->id;
					$Planilla->tipo_control = 'ingreso';
					$Planilla->actividad_ocupacional = $line[10];
					$Planilla->actividad_fisica = $line[11];
					$Planilla->frecuencia = $line[12];
					$Planilla->duracion = $line[13];
					$Planilla->intensidad = $line[14];
					$Planilla->talla = $line[15];
					$Planilla->peso = $line[16];
					$Planilla->peso_preg = $line[17];
					$Planilla->IMC = $line[18];
					$Planilla->cintura = $line[19];
					$Planilla->colesterol_total = $line[20];
					$Planilla->LDL = $line[21];
					$Planilla->HDL = $line[22];
					$Planilla->trigliceridos = $line[23];
					$Planilla->glicemia_ayuna = $line[24];
					$Planilla->presion_arterial_diastolica = $line[25];
					$Planilla->presion_arterial_sistolica = $line[26];
					$Planilla->test_6_minutos = $line[27];
					$Planilla->consulta_medica = NULL;
					$Planilla->nutricionista = NULL;
					$Planilla->psicologo = NULL;
					$Planilla->sesiones_actividad_fisica = NULL;
					$Planilla->save();
					//control 4 meses
					$parametros = ["pacientes_id" => $paciente->id,
					"tipo_control" => '4'];
					$Planilla = Control::firstOrNew($parametros);
					$Planilla->pacientes_id = $paciente->id;
					$Planilla->tipo_control = '4';
					$Planilla->actividad_ocupacional = $line[30];
					$Planilla->actividad_fisica = $line[31];
					$Planilla->frecuencia = $line[32];
					$Planilla->duracion = $line[33];
					$Planilla->intensidad = $line[34];
					$Planilla->talla = $line[35];
					$Planilla->peso = $line[36];
					$Planilla->peso_preg = NULL;
					$Planilla->IMC = $line[37];
					$Planilla->cintura = $line[38];
					$Planilla->colesterol_total = $line[39];
					$Planilla->LDL = $line[40];
					$Planilla->HDL = $line[41];
					$Planilla->trigliceridos = $line[42];
					$Planilla->glicemia_ayuna = $line[43];
					$Planilla->presion_arterial_sistolica = $line[44];
					$Planilla->presion_arterial_diastolica = $line[45];
					$Planilla->sesiones_actividad_fisica = $line[46];
					$Planilla->test_6_minutos = $line[47];
					$Planilla->consulta_medica = $line[48];
					$Planilla->nutricionista = $line[49].";".$line[51];
					$Planilla->psicologo = $line[50].";".$line[52];
					$Planilla->save();
					//control 6 meses
					$parametros = ["pacientes_id" => $paciente->id,
					"tipo_control" => '6'];
					$Planilla = Control::firstOrNew($parametros);
					$Planilla->pacientes_id = $paciente->id;
					$Planilla->tipo_control = '6';
					$Planilla->actividad_ocupacional = $line[55];
					$Planilla->actividad_fisica = $line[56];
					$Planilla->frecuencia = $line[57];
					$Planilla->duracion = $line[58];
					$Planilla->intensidad = $line[59];
					$Planilla->talla = $line[60];
					$Planilla->peso = $line[61];
					$Planilla->peso_preg = NULL;
					$Planilla->IMC = $line[62];
					$Planilla->cintura = $line[63];
					$Planilla->colesterol_total = NULL;
					$Planilla->LDL = NULL;
					$Planilla->HDL = NULL;
					$Planilla->trigliceridos = NULL;
					$Planilla->glicemia_ayuna = NULL;
					$Planilla->presion_arterial_sistolica = NULL;
					$Planilla->presion_arterial_diastolica = NULL;
					$Planilla->test_6_minutos = NULL;
					$Planilla->consulta_medica = NULL;
					$Planilla->nutricionista = NULL;
					$Planilla->psicologo = NULL;
					$Planilla->sesiones_actividad_fisica = NULL;
					$Planilla->save();
					//control 9 meses
					$parametros = ["pacientes_id" => $paciente->id,
					"tipo_control" => '9'];
					$Planilla = Control::firstOrNew($parametros);
					$Planilla->pacientes_id = $paciente->id;
					$Planilla->tipo_control = '9';
					$Planilla->actividad_ocupacional = $line[66];
					$Planilla->actividad_fisica = $line[67];
					$Planilla->frecuencia = $line[68];
					$Planilla->duracion = $line[69];
					$Planilla->intensidad = $line[70];
					$Planilla->talla = $line[71];
					$Planilla->peso = $line[72];
					$Planilla->peso_preg = NULL;
					$Planilla->IMC = $line[73];
					$Planilla->cintura = $line[74];
					$Planilla->colesterol_total = NULL;
					$Planilla->LDL = NULL;
					$Planilla->HDL = NULL;
					$Planilla->trigliceridos = NULL;
					$Planilla->glicemia_ayuna = NULL;
					$Planilla->presion_arterial_sistolica = NULL;
					$Planilla->presion_arterial_diastolica = NULL;
					$Planilla->test_6_minutos = NULL;
					$Planilla->consulta_medica = NULL;
					$Planilla->nutricionista = NULL;
					$Planilla->psicologo = NULL;
					$Planilla->sesiones_actividad_fisica = NULL;
					$Planilla->save();
					//control 12 meses
					$parametros = ["pacientes_id" => $paciente->id,
					"tipo_control" => '12'];
					$Planilla = Control::firstOrNew($parametros);
					$Planilla->pacientes_id = $paciente->id;
					$Planilla->tipo_control = '12';
					$Planilla->actividad_ocupacional = $line[77];
					$Planilla->actividad_fisica = $line[78];
					$Planilla->frecuencia = $line[79];
					$Planilla->duracion = $line[80];
					$Planilla->intensidad = $line[81];
					$Planilla->talla = $line[82];
					$Planilla->peso = $line[83];
					$Planilla->peso_preg = NULL;
					$Planilla->IMC = $line[84];
					$Planilla->cintura = $line[85];
					$Planilla->colesterol_total = NULL;
					$Planilla->LDL = NULL;
					$Planilla->HDL = NULL;
					$Planilla->trigliceridos = NULL;
					$Planilla->glicemia_ayuna = NULL;
					$Planilla->presion_arterial_sistolica = NULL;
					$Planilla->presion_arterial_diastolica = NULL;
					$Planilla->test_6_minutos = NULL;
					$Planilla->consulta_medica = NULL;
					$Planilla->nutricionista = NULL;
					$Planilla->psicologo = NULL;
					$Planilla->sesiones_actividad_fisica = NULL;
					$Planilla->save();
					echo "Creados y/o actualizados los controles del paciente ID =".$paciente->id."<BR>";
					
					
				}
			}
		}
		
	}else{
		echo "Documento Invalido";
	}
	
?>
