<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Planilla extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('Planilla', function($table)
		{
			$table->increments('id');
  			$table->integer('pacientes_id' );
		    $table->string('tipo_control');
		    $table->string('actividad_ocupacional');
		    $table->string('actividad_fisica');
		    $table->string('frecuencia');
		    $table->string('duracion');
		    $table->string('intensidad');
		    $table->decimal('talla');
		    $table->decimal('peso');
		    $table->decimal('peso_preg');
		    $table->decimal('IMC');
		    $table->decimal('cintura');
		    $table->integer('colesterol_total');
		    $table->integer('LDL');
		    $table->integer('HDL');
		    $table->integer('trigliceridos');
		    $table->integer('glicemia_ayuna');
		    $table->integer('presion_arterial_sistolica');
		    $table->integer('presion_arterial_diastolica');
		    $table->integer('test_6_minutos');
		    $table->integer('consulta_medica');
		    $table->integer('nutricionista');
		    $table->integer('psicologo');
		    $table->integer('sesiones_actividad_fisica');

		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('Planilla');
	}

}
