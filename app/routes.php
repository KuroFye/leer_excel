<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::get('inicio',function()
{
	return View::make('inicio');
});

Route::any('inicio',['as'=>'inicio',function(){
		$documento = Excel::load('C:\wamp\www\planilla.xls', false, 'UTF-8')->calculate()->setDateFormat('d-m-Y')->toArray(); 
		//dd($documento);
		//echo $documento['Pacientes'][0][1];
		return View::make('inicio')->with('documento',$documento);
	}]);
