<?php 
class Paciente extends Eloquent{

	protected $table = 'pacientes';

	public $timestamps = false;

	public function scopeRut($query,$rut)
	{
		return $query ->whereRut($rut);
	}

	public function controles()
	{
		return $this->hasMany('Control', 'pacientes_id');
	}
}
 ?>