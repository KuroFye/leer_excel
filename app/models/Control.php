<?php 
class Control extends Eloquent{

	protected $table = 'controles';
	protected $guarded = [''];

	public $timestamps = false;

	public function scopePacientes_id($query,$Pacientes_id)
	{
		return $query ->wherePacientes_id($Pacientes_id);
	}

	public function paciente()
	{
		return $this->belongsTo('Paciente', 'paciente_id');
	}

}

 ?>